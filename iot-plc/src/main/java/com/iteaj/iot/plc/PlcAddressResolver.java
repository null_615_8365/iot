package com.iteaj.iot.plc;

/**
 * plc读写操作地址解析
 */
public class PlcAddressResolver {

    /**
     * 起始地址
     */
    private int AddressStart = 0;

    /**
     * 获取数字的起始地址，也就是偏移地址
     * @return 值
     */
    public int getAddressStart() {
        return AddressStart;
    }

    /**
     * 设置数字的起始地址，也就是偏移地址
     * @param addressStart 值
     */
    public void setAddressStart(int addressStart) {
        AddressStart = addressStart;
    }

    /**
     * 设置起始地址的偏移地址信息，可以是正数，也可以是负数<br />
     * @param offset 偏移值
     */
    public void setAddressOffset( int offset ) { AddressStart += offset ;}

    /**
     * 获取取的数据长度，单位是字节还是字取决于设备方
     * @return 长度值
     */
    public int getLength() {
        return Length;
    }

    /**
     * 设置读取的数据长度，单位是字节还是字取决于设备方
     * @param length 长度值
     */
    public void setLength(int length) {
        Length = length;
    }

    private int Length = 0;

    @Override
    public String toString() {
        return String.valueOf(AddressStart);
    }
}
