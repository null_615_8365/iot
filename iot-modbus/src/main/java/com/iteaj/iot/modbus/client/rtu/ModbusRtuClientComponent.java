package com.iteaj.iot.modbus.client.rtu;

import com.iteaj.iot.FrameworkManager;
import com.iteaj.iot.IotProtocolFactory;
import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.IotClient;
import com.iteaj.iot.client.component.SimpleMultiClientManager;
import com.iteaj.iot.codec.filter.Interceptor;
import com.iteaj.iot.serial.SerialClient;
import com.iteaj.iot.serial.SerialComponent;
import com.iteaj.iot.serial.SerialConnectProperties;
import com.iteaj.iot.serial.SerialMessage;

public class ModbusRtuClientComponent<M extends ModbusRtuClientMessage>
        extends SimpleMultiClientManager implements ClientComponent<M> {

    private boolean start;
    private long startTime;
    private SerialComponent serialComponent;
    private static final String DESC = "基于Modbus Rtu协议客户端实现";


    public ModbusRtuClientComponent() {
        serialComponent = SerialComponent.instance();
    }

    public ModbusRtuClientComponent(SerialConnectProperties serialConnectProperties) {
        serialComponent = SerialComponent.instance(serialConnectProperties);
    }

    @Override
    public SerialConnectProperties getConfig() {
        return serialComponent.getConfig();
    }

    @Override
    public IotClient getClient() {
        return serialComponent.getClient();
    }

    @Override
    public SerialClient createNewClient(ClientConnectProperties config) {
        return serialComponent.createNewClient(config);
    }

    @Override
    public String getName() {
        return "ModbusRtuClient";
    }

    @Override
    public String getDesc() {
        return DESC;
    }

    @Override
    public boolean isStart() {
        return this.start;
    }

    @Override
    public long startTime() {
        return this.startTime;
    }

    @Override
    public void start(Object config) {
        this.start = true;
        ClientComponent clientComponent = FrameworkManager
                .getClientComponent(SerialMessage.class);

        // 之前没有注册需要先启动
        if(clientComponent == null) {
            serialComponent.start(config); // 启动串口组件
            this.startTime = serialComponent.startTime();
        }
    }

    @Override
    public void close() {
        this.serialComponent.close();
    }

    @Override
    public Interceptor getInterceptor() {
        return null;
    }

    @Override
    public IotProtocolFactory protocolFactory() {
        return null;
    }

}
