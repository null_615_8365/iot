package com.iteaj.iot.boot.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "iot.client")
public class IotClientProperties {

}
