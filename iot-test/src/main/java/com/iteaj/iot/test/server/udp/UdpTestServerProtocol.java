package com.iteaj.iot.test.server.udp;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageBody;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.udp.UdpMessageBody;

import java.io.IOException;
import java.net.InetSocketAddress;

public class UdpTestServerProtocol extends ServerInitiativeProtocol<UdpTestMessage> {

    private byte[] message;
    private InetSocketAddress recipient;

    public UdpTestServerProtocol(byte[] message) {
        this.message = message;
    }

    public UdpTestServerProtocol(byte[] message, InetSocketAddress recipient) {
        this.message = message;
        this.recipient = recipient;
    }

    @Override
    protected UdpTestMessage doBuildRequestMessage() throws IOException {
        DefaultMessageHead messageHead = new DefaultMessageHead(null, null, TestProtocolType.PIReq);
        messageHead.setMessage(this.message);
        return new UdpTestMessage(this.message, recipient);
    }

    @Override
    protected void doBuildResponseMessage(UdpTestMessage message) {

    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
