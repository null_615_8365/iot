package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.modbus.client.rtu.ModbusRtuClientMessage;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuBody;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuHeader;

public class ModbusRtuForDtuClientTestMessage extends ModbusRtuClientMessage {

    public ModbusRtuForDtuClientTestMessage(byte[] message) {
        super(message);
    }

    public ModbusRtuForDtuClientTestMessage(ModbusRtuHeader head) {
        super(head);
    }

    public ModbusRtuForDtuClientTestMessage(ModbusRtuHeader head, ModbusRtuBody body) {
        super(head, body);
    }

    @Override
    protected ModbusRtuHeader doBuild(byte[] message) {
        this.messageBody = ModbusRtuBody.buildRequestBody(message);
        this.messageHead = ModbusRtuHeader.buildRequestHeader(message);

        this.getHead().setType(getBody().getCode());
        this.getHead().setEquipCode(this.getEquipCode());
        return this.getHead();
    }
}
