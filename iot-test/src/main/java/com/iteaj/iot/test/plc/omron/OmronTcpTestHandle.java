package com.iteaj.iot.test.plc.omron;

import cn.hutool.core.util.RandomUtil;
import com.iteaj.iot.client.ClientProtocolHandle;
import com.iteaj.iot.plc.ReadAddress;
import com.iteaj.iot.plc.omron.OmronTcpProtocol;
import com.iteaj.iot.test.IotTestHandle;
import com.iteaj.iot.test.plc.TestPlcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class OmronTcpTestHandle implements ClientProtocolHandle<OmronTcpProtocol>, IotTestHandle {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object handle(OmronTcpProtocol protocol) {
        return null;
    }

    @Override
    public void start() throws Exception {
        long currentTimeMillis = System.currentTimeMillis();
        System.out.println("--------------------------------------- 开始欧姆龙PLC测试 ------------------------------------------");
        boolean randomBool = RandomUtil.randomInt(0, 2) == 0 ? false : true;
        new OmronTcpProtocol().write("D1.1", randomBool);
//        new OmronTcpProtocol().batchRead(Arrays.asList(ReadAddress.buildShortRead("D300")));

        Boolean aBoolean = new OmronTcpProtocol().readBool("D1.1");
        logger.info("布尔读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D1.1", randomBool, aBoolean, randomBool == aBoolean);

        short wshort = (short) RandomUtil.randomInt(10086);
        new OmronTcpProtocol().write("D2", wshort);
        short aShort = new OmronTcpProtocol().readInt16("D2");
        logger.info("Short读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D2", wshort, aShort, wshort == aShort);

        int uShort = RandomUtil.randomInt(Short.MAX_VALUE, Short.MAX_VALUE * 2);
        new OmronTcpProtocol().write("D5", Integer.valueOf(uShort).shortValue());
        Integer uInt16 = new OmronTcpProtocol().readUInt16("D5");
        logger.info("UShort读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D5", uShort, uInt16, uShort == uInt16);

        int winteger = RandomUtil.randomInt();
        new OmronTcpProtocol().write("D10", winteger);
        Integer integer = new OmronTcpProtocol().readInt32("D10");
        logger.info("Int读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D10", winteger, integer, winteger == integer);

        long uInt = RandomUtil.randomLong(Integer.MAX_VALUE, Long.valueOf(Integer.MAX_VALUE) * 2);
        new OmronTcpProtocol().write("D18", Long.valueOf(uInt).intValue());
        Long readUInt32 = new OmronTcpProtocol().readUInt32("D18");
        logger.info("UInt读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D18", uInt, readUInt32, uInt == readUInt32);

        long wlong = RandomUtil.randomLong();
        new OmronTcpProtocol().write("D22", wlong);
        Long aLong = new OmronTcpProtocol().readInt64("D22");
        logger.info("Long读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D22", wlong, aLong, wlong == aLong);

        float wfloat = (float) RandomUtil.randomDouble(164645.35);
        new OmronTcpProtocol().write("D30", wfloat);
        Float aFloat = new OmronTcpProtocol().readFloat("D30");
        logger.info("Float读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D30", wfloat, aFloat, wfloat == aFloat);

        double wdouble = RandomUtil.randomDouble(3, RoundingMode.CEILING);
        new OmronTcpProtocol().write("D38", wdouble);
        Double aDouble = new OmronTcpProtocol().readDouble("D38");
        logger.info("Double读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D38", wdouble, aDouble, wdouble == aDouble);

        String randomString = RandomUtil.randomString(8);
        new OmronTcpProtocol().write("D58", randomString);
        String s = new OmronTcpProtocol().readString("D58", (short) 4); // 这里读取的长度 = 8 / 2
        logger.info("String读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D58", randomString, s, randomString.equals(s));

        System.out.println("------------------------------------- 欧姆龙PLC连续区域块读写测试 ----------------------------------------");

        boolean[] booleans = TestPlcUtils.randomBools(11);
        new OmronTcpProtocol().write("W30", booleans);
        boolean[] booleans1 = new OmronTcpProtocol().readBool("W30", (short) 11);
        logger.info("boolean连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "W30", booleans, booleans1, TestPlcUtils.arrayEquals(booleans, booleans1));

        short[] shorts = TestPlcUtils.randomShorts(2);
        new OmronTcpProtocol().write("D68", shorts);
        short[] readInt16 = new OmronTcpProtocol().readInt16("D68", (short) 2);
        logger.info("Short连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D68", shorts, readInt16, TestPlcUtils.arrayEquals(shorts, readInt16));

        int[] randomInts = TestPlcUtils.randomInts(2);
        new OmronTcpProtocol().write("D78", randomInts);
        int[] readInt32 = new OmronTcpProtocol().readInt32("D78", (short) 2);
        logger.info("Int连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D78", randomInts, readInt32, TestPlcUtils.arrayEquals(randomInts, readInt32));

        long[] randomLongs = TestPlcUtils.randomLongs(2);
        new OmronTcpProtocol().write("D98", randomLongs);
        long[] readInt64 = new OmronTcpProtocol().readInt64("D98", (short) 2);
        logger.info("Long连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D98", randomLongs, readInt64, TestPlcUtils.arrayEquals(randomLongs, readInt64));

        float[] randomFloats = TestPlcUtils.randomFloats(2);
        new OmronTcpProtocol().write("D200", randomFloats);
        float[] readFloat = new OmronTcpProtocol().readFloat("D200", (short) 2);
        logger.info("Float连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D200", randomFloats, readFloat, TestPlcUtils.arrayEquals(randomFloats, readFloat));

        double[] randomDoubles = TestPlcUtils.randomDoubles(2);
        new OmronTcpProtocol().write("D250", randomDoubles);
        Double d250 = new OmronTcpProtocol().readDouble("D250");
        double[] readDouble = new OmronTcpProtocol().readDouble("D250", (short) 2);
        logger.info("Double连续读写测试 地址：{} - 写值：{} - 读值：{} - 测试状态：{}", "D250", randomDoubles, readDouble, TestPlcUtils.arrayEquals(randomDoubles, readDouble));

        System.out.println("------------------------------------- 欧姆龙PLC批量区域块读写测试 ----------------------------------------");
        new OmronTcpProtocol().batchRead(Arrays.asList(ReadAddress.buildShortRead("D300")));

        System.out.println("------------------------------- 欧姆龙PLC总测试时间：" + (System.currentTimeMillis() - currentTimeMillis) + " (ms) ---------------------------------");
        TimeUnit.SECONDS.sleep(2);
    }

    @Override
    public int getOrder() {
        return 1000 * 20;
    }
}
