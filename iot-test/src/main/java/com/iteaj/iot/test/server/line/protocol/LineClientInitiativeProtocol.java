package com.iteaj.iot.test.server.line.protocol;

import com.iteaj.iot.server.component.LineBasedFrameDecoderServerComponent;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.line.LineMessage;
import com.iteaj.iot.test.message.line.LineMessageBody;
import com.iteaj.iot.test.message.line.LineMessageHead;

/**
 * 测试{@link LineBasedFrameDecoderServerComponent}组件
 * 属于客户端主动请求服务端请求协议
 * @see TestProtocolType#CIReq
 */
public class LineClientInitiativeProtocol extends ClientInitiativeProtocol<LineMessage> {

    public LineClientInitiativeProtocol(LineMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected LineMessage doBuildResponseMessage() {
        LineMessageHead head = requestMessage.getHead();
        return new LineMessage(LineMessageHead.buildHeader(head.getEquipCode(), head.getMessageId()
                , head.getType(), head.getPayload()), LineMessageBody.build());
    }

    @Override
    protected void doBuildRequestMessage(LineMessage requestMessage) { }


    @Override
    public TestProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}
