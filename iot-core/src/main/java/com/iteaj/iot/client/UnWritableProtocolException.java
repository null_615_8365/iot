package com.iteaj.iot.client;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.ProtocolException;

/**
 * 协议不可写异常
 */
public class UnWritableProtocolException extends ProtocolException {

    private long bytesBeforeWritable;
    private long bytesBeforeUnwritable;

    /**
     * @see com.iteaj.iot.Protocol
     * @param protocol
     */
    public UnWritableProtocolException(Object protocol) {
        this("通道不可写", protocol);
    }

    public UnWritableProtocolException(String message, Object protocol) {
        super(message, protocol);
    }

    public UnWritableProtocolException(Object protocol, long bytesBeforeWritable, long bytesBeforeUnwritable) {
        super(protocol);
        this.bytesBeforeWritable = bytesBeforeWritable;
        this.bytesBeforeUnwritable = bytesBeforeUnwritable;
    }

    @Override
    public Protocol getProtocol() {
        return (Protocol) super.getProtocol();
    }

    public long getBytesBeforeWritable() {
        return bytesBeforeWritable;
    }

    public long getBytesBeforeUnwritable() {
        return bytesBeforeUnwritable;
    }
}
