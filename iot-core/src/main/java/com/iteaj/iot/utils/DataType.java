package com.iteaj.iot.utils;

public enum DataType {

    Byte(1),
    Short(2),
    UShort(2),
    Int(4),
    UInt(4),
    Long(8),
    Float(4),
    Double(8),
    String(-1), // -1表示自定义
    Boolean(0), // 0 表示无效
    ;
    /**
     * 字节长度
     */
    private int length;

    DataType(int length) {
        this.length = length;
    }

    public int getLength() {
        return length;
    }
}
